import os
import time


class Usbtmc(object):
    def __init__(self, device):
        self.device = device
        self.FILE = os.open(device, os.O_RDWR)

        # TODO: Test that the file opened

    def close(self):
        os.close(self.FILE)

    def write(self, command):
        os.write(self.FILE, command)
        # Reads straight after a write end up throwing a 'Connection Timed Out' exception without this delay :(
        time.sleep(0.001)

    def write_str(self, command):
        self.write(command.encode())

    def read(self, length=4000):
        return os.read(self.FILE, length)

    def read_str(self, length=4000):
        return self.read(length).decode()

    def get_name(self):
        self.write_str("*IDN?")
        return self.read_str(300)

    def send_reset(self):
        self.write_str("*RST")
