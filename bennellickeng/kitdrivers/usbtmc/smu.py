from __future__ import print_function
from .usbtmc import Usbtmc
import numpy as np
import time


class Smu(object):
  class SenseUnit:
    W = "WATT"
    # Add others from manual

  def __init__(self, device_file):
    self.device = Usbtmc(device_file)
    self.name = self.device.get_name()

    print(self.name)

  def output_on(self):
    self.device.write_str(":OUTPut:STATe ON")

  def output_off(self):
    self.device.write_str(":OUTPut:STATe OFF")

  def set_source_voltage(self, voltage):
    self.device.write_str(":SOUR:FUNC VOLT")
    self.device.write_str(":SOUR:VOLT {}".format(voltage))

  def set_source_voltage_current_limit(self, limit):
    self.device.write_str(":SOUR:VOLT:ILIMIT {}".format(limit))

  def set_sense_current(self):
    self.device.write_str(':SENSE:FUNC "CURR"')

  def set_sense_current_range(self, range=None):
    if range == None:
       self.device.write_str(':SENSE:CURR:RANG:AUTO ON')
    else:
       raise NotImplementedError("None auto sense range not implmented")

  def set_sense_current_unit(self, unit):
    self.device.write_str(":SENSE:CURR:UNIT {}".format(unit))
    
  def set_trigger_simple_loop(self, count, delay):
    self.device.write_str(':TRIG:LOAD "SimpleLoop", {}, {}'.format(count, delay))
    
  def trigger_and_collect(self, number_of_points, wait_time):
    self.device.write_str("INIT")
    self.device.write_str("*WAI")
    self.device.write_str(':TRAC:DATA? 1, {}, "defbuffer1", TST, READ'.format(number_of_points))

    # Urgh
    time.sleep(wait_time)

    data = smu._read_str(10000000).strip('\n')
    text = (data.split(','))
    return [text[x:x+2] for x in range(0, len(text), 2)]

  def _read_str(self, length=4000):
    """Read an arbitrary amount of data directly from the smu"""
    return self.device.read_str(length)

  def reset(self):
    """Reset the instrument"""
    self.device.send_reset()
